extends Node

var item_data: Dictionary

func _ready() -> void:
	item_data = load_data("res://data/ItemData.json")

func load_data(file_path: String) -> Dictionary:
	var json_data
	var file_data = File.new()
	
	file_data.open(file_path, File.READ)
	json_data = JSON.parse(file_data.get_as_text())
	file_data.close()
	
	if json_data.error == OK:
		return json_data.result
	else:
		print(json_data.error_line)
		print(json_data.error_string)
		return json_data.result
