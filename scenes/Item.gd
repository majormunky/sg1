extends Node2D

var item_name
var item_amount = 0
onready var label = $Label

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass

func setup(name: String, amount: int) -> void:
	item_name = name
	match name:
		"Iron Ore":
			$TextureRect.texture = load("res://assets/images/meteorGrey_small1.png")
		"Copper Ore":
			$TextureRect.texture = load("res://assets/images/meteorBrown_small1.png")
	self.add_item_amount(amount)

func add_item_amount(amount_to_add):
	item_amount += amount_to_add
	if label:
		label.text = String(item_amount)

func decrease_item_amount(amount_to_remove):
	item_amount -= amount_to_remove
	if label:
		label.text = String(item_amount)
