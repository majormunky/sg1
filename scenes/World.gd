extends Node2D

onready var ui = $UI
onready var rock_group = $Rocks
var iron_positions = [
	Vector2(100, 200),
	Vector2(250, 400)
]
var copper_positions = [
	Vector2(400, 500),
]
var IronRock = preload("res://scenes/IronRock.tscn")
var CopperRock = preload("res://scenes/CopperRock.tscn")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_process_input(true)
	for pos in iron_positions:
		var new_rock = IronRock.instance()
		new_rock.position = pos
		rock_group.add_child(new_rock)
	for pos in copper_positions:
		var new_rock = CopperRock.instance()
		new_rock.position = pos
		rock_group.add_child(new_rock)

func _input(event: InputEvent) -> void:
	if event.is_action_type() && event.is_action_pressed("menu"):
		if ui.visible:
			ui.visible = false
		else:
			ui.visible = true
