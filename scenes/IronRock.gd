extends Area2D

onready var sprite = $Sprite
var large_rock_image = preload("res://assets/images/meteorGrey_big1.png")
var med_rock_image = preload("res://assets/images/meteorGrey_med1.png")
var RockPickup = preload("res://scenes/RockPickup.tscn")
var pickup_group = null
var item_name = "Iron Ore"
var item_amount = 15
var current_hp = 3
var max_hp = 3
var current_size = "large"
var rock_spread = 40

func _ready() -> void:
	pickup_group = get_parent().get_parent().find_node("Pickups")
	
func create_rock_pickup() -> void:
	var rg = RandomNumberGenerator.new()
	rg.randomize()
	for _i in range(3):
		var rp = RockPickup.instance()
		var random_dir = Vector2(
			rg.randi_range(-rock_spread, rock_spread),
			rg.randi_range(-rock_spread, rock_spread)
		)
		rp.setup(item_name, item_amount)
		rp.position = global_position + random_dir
		pickup_group.add_child(rp)

func get_hit() -> void:
	current_hp -= 1
	if current_hp <= 0:
		match current_size:
			"large":
				current_hp = max_hp
				sprite.texture = med_rock_image
				call_deferred("create_rock_pickup")
				current_size = "medium"
			"medium":
				call_deferred("create_rock_pickup")
				queue_free()
