extends KinematicBody2D

onready var animated_sprite = $AnimatedSprite
var inventory = null
var Bullet = preload("res://scenes/Bullet.tscn")
var RockPickup = preload("res://scenes/RockPickup.gd")
var velocity: Vector2
var speed: float = 100.0
var direction = "down"
var is_walking = false
var is_shooting = false

func _ready() -> void:
	set_process_input(true)

func _input(event: InputEvent) -> void:
	if event.is_action_type() && event.is_action_pressed("shoot"):
		shoot()

func _physics_process(_delta: float) -> void:
	velocity = Vector2.ZERO
	is_walking = false
	if Input.is_action_pressed("run_down"):
		velocity.y = speed
		direction = "down"
		is_walking = true
	elif Input.is_action_pressed("run_up"):
		velocity.y = -speed
		direction = "up"
		is_walking = true
	if Input.is_action_pressed("run_left"):
		velocity.x = -speed
		direction = "left"
		is_walking = true
	elif Input.is_action_pressed("run_right"):
		velocity.x = speed
		direction = "right"
		is_walking = true
	
	update_animation()
	velocity = move_and_slide(velocity)

func update_animation() -> void:
	if is_shooting:
		return
	if direction == "down" && is_walking:
		animated_sprite.set_animation("run_down")
	else:
		animated_sprite.set_animation("idle")

func shoot() -> void:
	animated_sprite.set_animation("shoot_down")
	is_shooting = true
	var b = Bullet.instance()
	
	match direction:
		"down":
			b.setup(Vector2.DOWN)
		"up":
			b.setup(Vector2.UP)
		"left":
			b.setup(Vector2.LEFT)
		"right":
			b.setup(Vector2.RIGHT)
	
	b.position = position
	get_parent().add_child(b)
	yield(get_tree().create_timer(0.5), "timeout")
	is_shooting = false

func add_item(name: String, amount: int) -> void:
	get_parent().find_node("Inventory").add_item(name, amount)

func _on_PickupDetector_body_entered(body: Node) -> void:
	var item_to_pickup = body as RockPickup
	item_to_pickup.pick_up_item(self)
