extends Node2D
const SlotClass = preload("res://scenes/Slot.gd")
const ItemClass = preload("res://scenes/Item.tscn")
onready var inventory_slots = $GridContainer
var item_description = null
var holding_item = null

func _ready() -> void:
	item_description = get_parent().find_node("ItemDescription")
	set_process_input(true)
	for inv_slot in inventory_slots.get_children():
		inv_slot.connect("gui_input", self, "slot_gui_input", [inv_slot])
		inv_slot.connect("mouse_entered", self, "on_mouse_entered", [inv_slot])
		inv_slot.connect("mouse_exited", self, "on_mouse_exit", [inv_slot])

func on_mouse_entered(slot: SlotClass):
	if slot.item:
		var desc = slot.item.item_name + "\n\n" + JsonData.item_data[slot.item.item_name]["description"]
		item_description.bbcode_text = desc

func on_mouse_exit(slot: SlotClass):
	item_description.bbcode_text = ""

func add_item(name: String, amount: int) -> void:
	print("Trying to add item " + name + " with amount: " + str(amount))
	var amount_left = amount
	
	while amount_left > 0:
		for inv_slot in inventory_slots.get_children():
			if inv_slot.item && inv_slot.item.item_name == name:
				# this is how big the stack can be
				var stack_size = int(JsonData.item_data[name]["stack_size"])
				print("Stack Size: " + str(stack_size))
				
				# this is how many i can add to the existing stack
				var able_to_add = stack_size - inv_slot.item.item_amount
				
				# check to see if what is possible to add is greater than what we have
				if able_to_add >= amount:
					# if so, we can just add to the stack
					inv_slot.item.add_item_amount(amount)
					print("Added item to slot " + inv_slot.name + ", current: " + str(inv_slot.item.item_amount))
					return
				else:
					# if not, then we have to add what we can, then add the rest to a new stack
					print("Able to add: " + str(able_to_add))
					print("Amount: " + str(amount))
					amount_left = amount - able_to_add
					print("Amount Left: " + str(amount_left))
					var amount_to_add = amount - amount_left
					inv_slot.item.add_item_amount(amount_to_add)
					print("We added some to a pile and have " + str(amount_left) + " left")
		
		print("Unable to find an already filled slot with " + name)
		for inv_slot in inventory_slots.get_children():
			if inv_slot.item == null:
				var new_item = ItemClass.instance()
				inv_slot.add_child(new_item)
				new_item.setup(name, amount_left)
				inv_slot.item = new_item
				inv_slot.refresh_style()
				print("Found slot for item: " + inv_slot.name)
				return

func slot_gui_input(event: InputEvent, slot: SlotClass):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT && event.pressed:
			if holding_item != null:
				print("We are holding an item")
				if !slot.item:
					print("item put into slot")
					slot.put_into_slot(holding_item)
					holding_item = null
				else:
					if holding_item.item_name != slot.item.item_name:	
						var temp_item = slot.item
						slot.pick_from_slot()
						temp_item.global_position = event.global_position
						slot.put_into_slot(holding_item)
						holding_item = temp_item
					else:
						var stack_size = int(JsonData.item_data[slot.item.item_name]["stack_size"])
						var able_to_add = stack_size - slot.item.item_amount
						if able_to_add >= holding_item.item_amount:
							slot.item.add_item_amount(holding_item.item_amount)
							holding_item.queue_free()
							holding_item = null
						else:
							slot.item.add_item_amount(able_to_add)
							holding_item.decrease_item_amount(able_to_add)
			elif slot.item:
				print("Pick up item")
				holding_item = slot.item
				slot.pick_from_slot()
				holding_item.global_position = get_global_mouse_position() - Vector2(15, 15)

func _input(_event: InputEvent) -> void:
	if holding_item:
		holding_item.global_position = get_global_mouse_position() - Vector2(15, 15)

