extends Area2D

var speed = 750
var velocity: Vector2

func setup(linear_velocity: Vector2) -> void:
	velocity = linear_velocity

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

func _physics_process(delta: float) -> void:
	position += velocity * speed * delta

func _on_VisibilityNotifier2D_screen_exited() -> void:
	# when the bullet goes off screen, we need to destroy it
	queue_free()

func _on_Bullet_area_entered(area: Area2D) -> void:
	if area.is_in_group("rocks"):
		area.get_hit()
	queue_free()
