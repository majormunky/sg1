extends Panel

var TestItem = preload("res://scenes/Item.tscn")
var default_background = preload("res://assets/images/metalPanel.png")
var empty_background = preload("res://assets/images/metalPanelTransparent.png")
var default_style: StyleBoxTexture = null
var empty_style: StyleBoxTexture = null
var item = null

func _ready() -> void:
	default_style = StyleBoxTexture.new()
	default_style.texture = default_background
	empty_style = StyleBoxTexture.new()
	empty_style.texture = empty_background
	refresh_style()

func refresh_style() -> void:
	if item == null:
		set("custom_styles/panel", empty_style)
	else:
		set("custom_styles/panel", default_style)

func pick_from_slot() -> void:
	remove_child(item)
	var parent_node = find_parent("Inventory")
	parent_node.add_child(item)
	item = null
	refresh_style()

func put_into_slot(new_item) -> void:
	print("put into slot")
	item = new_item
	item.position = Vector2(0, 0)
	var parent_node = find_parent("Inventory")
	parent_node.remove_child(item)
	add_child(item)
	refresh_style()


