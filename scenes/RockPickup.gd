extends KinematicBody2D
class_name RockPickup

var item_amount = null
var item_name = null
var getting_picked_up = false
var velocity = Vector2.ZERO
const ACCELERATION = 460
const MAX_SPEED = 225 
var player = null

func _ready() -> void:
	pass # Replace with function body.

func setup(name: String, amount: int):
	item_name = name
	item_amount = amount
	match name:
		"Iron Ore":
			$Sprite.texture = load("res://assets/images/meteorGrey_small1.png")
		"Copper Ore":
			$Sprite.texture = load("res://assets/images/meteorBrown_small1.png")

func _physics_process(delta: float) -> void:
	if getting_picked_up:
		var direction = global_position.direction_to(player.global_position)
		velocity = velocity.move_toward(direction * MAX_SPEED, ACCELERATION * delta)
		velocity = move_and_slide(velocity)

		var distance = global_position.distance_to(player.global_position)
		if distance < 8:
			player.add_item(item_name, item_amount)
			queue_free()

func pick_up_item(body):
	print("Pick up item")
	player = body
	getting_picked_up = true
